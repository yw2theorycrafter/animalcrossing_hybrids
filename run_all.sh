#!/bin/bash
set -beEu -o pipefail
#./run.sh Pansy Purple
#./run.sh Rose Blue
#./run.sh Mum Green
#./run.sh Tulip Purple
#./run.sh Hyacinth Purple
#./run.sh Cosmos Black
#./run.sh Yuri Black
#./run.sh Anemones Purple
#./run.sh Anemones Master

#./run.sh Rose Universal
#./run.sh Mum Universal
#./run.sh Pansy Universal
#./run.sh Cosmos Universal
./run.sh Anemones Universal
#Trivial:
#./run.sh Tulip Universal
#./run.sh Hyacinth Universal
#./run.sh Yuri Universal
