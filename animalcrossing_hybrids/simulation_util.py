import numpy as np
from animalcrossing_hybrids.flower_models import *
from animalcrossing_hybrids.recipes import *
from animalcrossing_hybrids.flower import *
from animalcrossing_hybrids.simulation_util import *
from multiprocessing import Pool
import logging
import argparse
from itertools import cycle
import hickle as hkl
import os
from tqdm import tqdm


def initialConditions(model):
    # Pick 32 of each seed flower.
    toRet = FlowerFarm()
    for seed in model.seeds:
        toRet.flowers += [Flower(seed[1], seed[0])] * 32
    return toRet


def simulation_runonce(model, recipe, target, nsteps):
    farm = initialConditions(model)
    successSteps = None
    numFlowersWatered = 0
    crossings = []
    for i in range(nsteps):
        newOffspring = runRecipe(recipe, model, farm, crossings)
        numFlowersWatered += len(newOffspring) * 10  # approximation
        # Check if we are done
        done = False
        for offspring in newOffspring:
            # First case handles the Universal case.
            if offspring.label == target or model.color(offspring.genotype) == target:
                successSteps = i + 1
                done = True
                break
        if done:
            break
    return (successSteps, numFlowersWatered, farm, crossings)


class _simulation:
    def __init__(self, model, recipe, target, nsteps):
        self.model = model
        self.recipe = recipe
        self.target = target
        self.nsteps = nsteps

    def __call__(self, args):
        return simulation_runonce(self.model, self.recipe, self.target, self.nsteps)


def simulation(model, recipe, nruns, target, nsteps=1000):
    with Pool(4) as p:
        results = list(
            tqdm(
                p.imap_unordered(
                    _simulation(model, recipe, target, nsteps), range(nruns)
                ),
                total=nruns,
            )
        )
        return results
