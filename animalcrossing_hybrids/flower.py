import random


class Flower:
    def __init__(self, genotype, label):
        self.genotype = genotype
        self.label = label

    def __repr__(self):
        return f"{self.label}_{self.genotype}"


# Crosses two single gene genotypes (strings)
def _crossGenotype(a, b):
    result = ["0", "0"]
    # Take one allele from each parent
    result[0] = random.choice([a[0], a[1]])
    result[1] = random.choice([b[0], b[1]])
    result = "".join(result)
    # Sort result
    if result == "10":
        return "01"
    return result


def crossGenotype(a, b):
    result = "".join(
        [
            _crossGenotype(a[gene * 2 : gene * 2 + 2], b[gene * 2 : gene * 2 + 2])
            for gene in range(len(a) // 2)
        ]
    )
    result = "".join(result)
    return result


# Crosses two flowers
# def cross(a, b):
#    a = a.genotype
#    b = b.genotype
#    result = crossGenotype(a, b)
#    return Flower(result)


class FlowerFarm:
    def __init__(self):
        self.flowers = []


def colorStrings(model, farm):
    return [x.label for x in farm.flowers]
