import csv
import logging


class FlowerModel:
    def __init__(self, flower_type):
        self.geneToColor = {}
        self.colorToGenes = {}
        self.seeds = []
        with open("data/flower_genes.tsv", "r") as tsvin:
            for row in csv.DictReader(tsvin, delimiter="\t"):
                if row["Type"] != flower_type:
                    continue
                genes = ["Red", "Yellow", "White"]
                # Only roses use the brightness gene
                if flower_type == "Rose":
                    genes.append("Brightness")
                genotype = []
                isUniversal = True
                for gene in genes:
                    _gene = int(row[gene])
                    if _gene == 0:
                        genotype += ["0", "0"]
                        isUniversal = False
                    if _gene == 1:
                        genotype += ["0", "1"]
                    if _gene == 2:
                        genotype += ["1", "1"]
                        isUniversal = False
                genotype = "".join(genotype)
                # isUniversal = genotype == "010101"
                if isUniversal:
                    self.universalGenotype = genotype
                    self.universalColor = row["Color"]
                self.geneToColor[genotype] = row["Color"]
                if int(row["Seed Bag"]) == 1:
                    self.seeds.append((row["Color"], genotype))
                genes = self.colorToGenes.get(row["Color"], [])
                genes.append(genotype)
                self.colorToGenes[row["Color"]] = genes

    def colors(self):
        return list(self.colorToGenes.keys())

    def genes(self, color):
        return self.colorToGenes[color]

    def color(self, genotype):
        return self.geneToColor[genotype]
