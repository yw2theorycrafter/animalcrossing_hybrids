import numpy as np
from animalcrossing_hybrids.flower_models import *
from animalcrossing_hybrids.recipes import *
from animalcrossing_hybrids.flower import *
from animalcrossing_hybrids.simulation_util import *
from multiprocessing import Pool
import logging
import argparse
from itertools import cycle
import hickle as hkl
import os
from tqdm import tqdm

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

logger.setLevel(logging.INFO)

parser = argparse.ArgumentParser(description="Run ACNH hybrid simulations.")
parser.add_argument("-o", "--out", required=True, help="Output prefix")
parser.add_argument(
    "-f", "--flower", required=True, help="Flower type (Rose, Mume etc.)"
)
parser.add_argument("-t", "--target", required=True, help="Target color (Blue, etc.)")
parser.add_argument("-m", "--mode", required=True, help="Either simulate or optimize")
parser.add_argument(
    "-nsims", "--nsims", required=False, default=100, help="Number of simulations"
)

args = parser.parse_args()


def summarizeStrings(strings):
    occurrences = {}
    for x in strings:
        count = occurrences.get(x, 0)
        occurrences[x] = count + 1
    return [f"{v} x {k}" for k, v in occurrences.items()]


def _optimize(model, target, nsteps, recipe=None):
    # If no initial recipe provided, make a new one:
    if recipe == None:
        recipe = OptimizedRecipe(model)
        if target.endswith("Universal"):
            recipe.labels[target] = [model.universalGenotype]
    farm = initialConditions(model)
    successSteps = None
    for i in range(nsteps):
        recipe.add_random_rule(model, farm)
        newOffspring = runRecipe(recipe, model, farm)
        # Check if we are done
        done = False
        for offspring in newOffspring:
            # if model.color(offspring.genotype) == target:
            if offspring.label == target:
                successSteps = i + 1
                done = True
                break
        if done:
            break
    if successSteps is not None:
        recipe = minimize_optimized_recipe(model, recipe, target)
        # ruleStrings = "\n".join(sorted([str(x) for x in recipe.rules]))
        # labelStrings = "\n".join(sorted([str(x) for x in recipe.labels.items()]))
        # logger.info(f"Recipe:\n{ruleStrings} {labelStrings}")
        # logger.info(
        #    f"Garden at step {successSteps}: "
        #    + ", ".join(summarizeStrings(colorStrings(model, farm)))
        # )
        return (successSteps, farm, recipe)
    else:
        # logger.info("Discovery step failed (will try again.)")
        return None


# See https://github.com/tqdm/tqdm/issues/228 for other styles
bar_format_ind_loop = cycle(list("/-\|"))


def optimize(model, nruns, target, nsteps=100):
    if target == "Universal":
        target = f"{model.universalColor}Universal"

    best = bestRecipes(f"{args.out}-best-recipes.hdf5")
    while len(best.topK) > best.N // 2:
        best.topK.pop()
    # Minimize each recipe, as minimization can get better over time:
    # Re-score them too. First addition will fix sorted order.
    def score_recipe(recipe):
        simsteps = nsteps * 2
        scores = simulation(model, recipe, 50, target, simsteps)
        time_score = np.array([x[0] if x[0] is not None else simsteps for x in scores])
        scale_score = np.array([x[1] if x[0] is not None else 9999 for x in scores])
        # score = math.sqrt(
        #    np.percentile(time_score, 95) * np.percentile(scale_score, 95)
        # )
        score = np.percentile(time_score, 95)
        # Add a penalty for complexity
        # score = score * math.pow(1 / 0.95, len(recipe.rules))
        return score, recipe

    # Consider known recipes:
    if True and target == "Blue" and args.flower == "Rose":
        for name, recipe in [
            ("#1", RoseRecipe1()),
            ("#2", RoseRecipe2()),
            ("#3", RoseRecipe3()),
            ("Paleh", PalehRoseRecipe()),
        ]:
            score, recipe = score_recipe(make_optimized_recipe(model, recipe))
            logging.info(recipe)
            logging.info(f"Known recipe {name} had score {score}")
            best.add(score, recipe)

    best.topK = [
        score_recipe(minimize_optimized_recipe(model, recipe, target))
        # (score, minimize_optimized_recipe(model, recipe, target))
        for score, recipe in best.topK
    ]
    logging.info(best)

    logging.info(best)
    t = tqdm(
        total=1, bar_format=f"Searching for candidates...{next(bar_format_ind_loop)}"
    )
    for i in range(nruns):
        if i % 10 > 7 and len(best.topK) > 0:
            t.bar_format = f"Minimizing known best...{next(bar_format_ind_loop)}"
            t.update(0)
            for j in range(10):
                score, recipe = random.choice(best.topK)
                small_recipe = minimize_optimized_recipe(model, recipe, target)
                small_recipe.rules.pop(random.randrange(len(small_recipe.rules)))
                small_recipe = minimize_optimized_recipe(model, small_recipe, target)
                result = simulation_runonce(model, small_recipe, target, nsteps)
                if result[0] is None:
                    small_recipe = None
                if small_recipe is not None:
                    break
            if small_recipe is not None:
                score, recipe = score_recipe(make_optimized_recipe(model, recipe))
                logging.info(f"Candidate had score {score}")
                best.add(score, recipe)
        elif i % 10 > 4 and len(best.topK) > 0:
            t.bar_format = f"Extending known best...{next(bar_format_ind_loop)}"
            t.update(0)

            score, recipe = random.choice(best.topK)
            try:
                recipe = minimize_optimized_recipe(model, recipe, target)
                result = _optimize(model, target, nsteps, recipe)
            except Exception as e:
                logging.info("Couldn't extend known best, obsolete format.")
                continue

            if result is None:
                continue
            recipe = result[2]
        else:
            t.bar_format = f"Searching for candidates...{next(bar_format_ind_loop)}"
            t.update(0)
            result = _optimize(model, target, nsteps)
            if result is None:
                continue
            recipe = result[2]

        # Limit recipes to 10 rules.
        for i in range(10):
            small_recipe = minimize_optimized_recipe(model, recipe, target)
            while len(small_recipe.rules) > 10:
                small_recipe.rules.pop(random.randrange(len(small_recipe.rules)))
                small_recipe = minimize_optimized_recipe(model, small_recipe, target)
            result = simulation_runonce(model, small_recipe, target, nsteps)
            if result[0] is None:
                small_recipe = None
            else:
                break
        if small_recipe is None:
            continue
        recipe = small_recipe

        t.close()
        logging.info(f"Found a candidate, testing...")
        score, recipe = score_recipe(minimize_optimized_recipe(model, recipe, target))
        logging.info(f"Candidate had score {score}")
        best.add(score, recipe)
        t = tqdm(
            total=1,
            bar_format=f"Searching for candidates...{next(bar_format_ind_loop)}",
        )
    t.close()
    return best.topK


logger.setLevel(logging.INFO)

desiredFlower = args.flower

model = FlowerModel(desiredFlower)

if args.mode == "optimize":
    results = optimize(model, int(args.nsims), args.target)
    logging.info(results)

if args.mode == "run_recipe":
    if desiredFlower == "Rose":

        # results = simulation(model, PalehRoseRecipe(), roseInitial, int(args.nsims))
        # results = simulation(model, RoseRecipe2(), roseInitial, int(args.nsims))
        # results = simulation(model, RoseRecipe1(), roseInitial, int(args.nsims))
        results = simulation(model, RoseRecipe3(), int(args.nsims), "Blue")
        # results = simulation(
        #    model, uaverageidolRoseRecipe(), roseInitial, int(args.nsims), "Blue"
        # )

    if desiredFlower == "tulip":
        model = FlowerModel("data/Tulip.tsv")

        def initial():
            toRet = FlowerFarm()
            # Red seeds
            toRet.flowers += [Flower("110001")] * 10
            # Yellow seeds
            toRet.flowers += [Flower("001100")] * 10
            # White seeds
            toRet.flowers += [Flower("000001")] * 10
            return toRet

        results = simulation(model, TulipRecipe(), initial, int(args.nsims), "Purple")
        # results = simulation(model, TulipRecipe2(), initial, int(args.nsims), "Purple")

    if desiredFlower == "pansy":
        model = FlowerModel("data/nl_pansies.tsv")

        def roseInitial():
            toRet = FlowerFarm()
            # Red seeds
            toRet.flowers += [Flower("110000")] * 10
            # Yellow seeds
            toRet.flowers += [Flower("001100")] * 10
            # White seeds
            toRet.flowers += [Flower("000001")] * 10
            return toRet

        # results = simulation(model, PurplePansyRecipe1(), roseInitial, int(args.nsims))

    with open(f"{args.out}-metrics.tsv", "w") as tsvout:
        writer = csv.writer(tsvout, delimiter="\t")
        for result in results:
            writer.writerow((result[0], result[1]))

    with open(f"{args.out}-composition.tsv", "w") as tsvout:
        writer = csv.writer(tsvout, delimiter="\t")
        for result in results:
            for flower in result[2].flowers:
                writer.writerow((colorString(model, flower), flower.genotype))
