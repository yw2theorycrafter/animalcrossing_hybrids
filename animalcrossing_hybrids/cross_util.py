# Crosses two single gene genotypes (strings)
def _dcrossGenotype(a, b, d):
    result = ["0", "0"]
    # Take one allele from each parent
    result[0] = a[1] if d.pop() else a[0]
    result[1] = b[1] if d.pop() else b[0]
    result = "".join(result)
    # Sort result
    if result == "10":
        return "01"
    return result


def dcrossGenotype(a, b, d):
    result = "".join(
        [
            _dcrossGenotype(a[gene * 2 : gene * 2 + 2], b[gene * 2 : gene * 2 + 2], d)
            for gene in range(len(a) // 2)
        ]
    )
    result = "".join(result)
    return result


def intToBits(i):
    return [x == "1" for x in format(i, "08b")]


# returns a set
def allCrossings(As, Bs):
    results = set()
    for a in As:
        for b in Bs:
            for i in range(256):
                results.add(dcrossGenotype(a, b, intToBits(i)))
    return results
