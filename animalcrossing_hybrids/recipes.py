from animalcrossing_hybrids.flower import *
import logging
import hickle as hkl
import os
import math

import random

# import numpy.random as random
from animalcrossing_hybrids.cross_util import allCrossings

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class CrossRule:
    def __init__(
        self,
        input1,
        input2,
        output1,
        output2=None,
        output3=None,
        output4=None,
        output5=None,
        output6=None,
        output7=None,
        output8=None,
        output9=None,
    ):
        self.input1 = input1
        self.input2 = input2
        self.output = [output1]
        if output2:
            self.output.append(output2)
        if output3:
            self.output.append(output3)
        if output4:
            self.output.append(output4)
        if output5:
            self.output.append(output5)
        if output6:
            self.output.append(output6)
        if output7:
            self.output.append(output7)
        if output8:
            self.output.append(output8)
        if output9:
            self.output.append(output9)

    # Checks whether two input flowers match the input of the rule
    def matches(self, model, a, b):
        a = a.label
        b = b.label
        return (a == self.input1 and b == self.input2) or (
            a == self.input2 and b == self.input1
        )

    def run(self, model, offspring, frequencies, crossings, recipe):
        if self.input1 not in frequencies or self.input2 not in frequencies:
            return

        nRules1 = len(
            list(
                filter(
                    lambda x: x.input1 == self.input1 or x.input2 == self.input1,
                    recipe.rules,
                )
            )
        )
        nRules2 = len(
            list(
                filter(
                    lambda x: x.input1 == self.input2 or x.input2 == self.input2,
                    recipe.rules,
                )
            )
        )
        # nSplit = math.ceil(len(farm_frequencies[color]) / max(nRules, 1))
        # farm_frequencies[color] = random.sample(farm_frequencies[color], nSplit)
        if self.input1 == self.input2:
            As = frequencies[self.input1]
            numCrosses = len(As) // nRules1 // 2
            # As = random.sample(As, numCrosses * 2)
            crosses = list(zip(As[0:numCrosses], As[numCrosses : numCrosses * 2]))
        else:
            As = frequencies[self.input1]
            Bs = frequencies[self.input2]
            numCrosses = min(len(As) // nRules1, len(Bs) // nRules2)
            crosses = list(
                # zip(random.sample(As, numCrosses), random.sample(Bs, numCrosses))
                zip(As[0:numCrosses], Bs[0:numCrosses])
            )

        for a, b in crosses:
            # Model self-cloning, when a, b, and c are the same color we can't use blockers
            # this seems to be relatively rare so maybe 5% of the time
            if (
                model.color(a.genotype) == model.color(b.genotype)
                and random.random() < 0.05
            ):
                genotype = random.choice([a.genotype, b.genotype])
            else:
                genotype = crossGenotype(a.genotype, b.genotype)
            color = model.color(genotype)
            # Only 20% of crosses survive, to achieve a net
            # yield of 1/10 offspring to watered flowers
            if random.random() < 0.2:
                # Does the recipe permit the offspring?
                label = "Other"
                allowed = False
                for _output in self.output:
                    if _output.startswith(color):
                        # Hack
                        if color == "Red" and _output.startswith("RedYellow"):
                            continue
                        label = _output
                        allowed = True
                        break
                if allowed:
                    offspring.append(Flower(genotype, label))
                if crossings is not None:
                    crossings.append((a, b, label))

    def needs(self, model, flower):
        cstr = flower.label
        return self.input1 == cstr or self.input2 == cstr

    def __repr__(self):
        return f"{self.input1}x{self.input2} = {self.output}"


def runRecipe(recipe, model, farm, crossings=None):
    offspring = []

    farm_frequencies = {}
    for flower in farm.flowers:
        color = flower.label
        flowers = farm_frequencies.get(color, [])
        flowers.append(flower)
        farm_frequencies[color] = flowers

    # Split the frequencies over how many recipes use that rule
    # for color in farm_frequencies:
    # nRules = len(list(filter(lambda x: color in x.output, recipe.rules)))
    # nSplit = math.ceil(len(farm_frequencies[color]) / max(nRules, 1))
    # farm_frequencies[color] = random.sample(farm_frequencies[color], nSplit)

    # Focus on recipes that can be done with modest space:
    max_farm_size = 32
    # solo flowers duplicate with 10% frequency
    for color, flowers in farm_frequencies.items():
        if len(flowers) == 1:
            if random.random() < 0.1:
                farm.flowers.append(flowers[0])

    for rule in recipe.rules:
        # Don't run a rule if we have plenty of all relevant results
        needsRun = False
        for output in rule.output:
            if (
                output not in recipe.irrelevantLabels
                and len(farm_frequencies.get(output, [])) < max_farm_size
            ):
                needsRun = True
                break
        if not needsRun:
            continue

        rule.run(model, offspring, farm_frequencies, crossings, recipe)

    # Cap flowers per label
    offspring = list(
        filter(
            lambda x: len(farm_frequencies.get(x.label, [])) < max_farm_size, offspring
        )
    )
    farm.flowers += offspring
    # logger.warn("New generation: " + ", ".join(colorStrings(model, farm)))
    return offspring


def is_seed_genotype(model, genotype):
    for seed in model.seeds:
        if genotypes == seed[1]:
            return True
    return False


class OptimizedRecipe:
    def __init__(self, model):
        self.rules = []
        # Label the seed types:
        self.labels = {}
        for seed in model.seeds:
            self.labels[seed[0]] = [seed[1]]
        self.irrelevantLabels = set()

    def has_rule(self, cross):
        for rule in self.rules:
            if (rule.input1 == cross[0] and rule.input2 == cross[1]) or (
                rule.input2 == cross[0] and rule.input1 == cross[1]
            ):
                return True
        return False

    def get_genotype_label(self, genotype):
        for label, genotypes in self.labels.items():
            if len(genotypes) == 1 and genotypes[0] == genotype:
                return label
        return None

    def make_longer_label(self, prefix):
        label = prefix
        while label in self.labels:
            label += "Hybrid"
        return label

    # Adds a rule. All possible color outputs of the rule are used, and given a unique
    # label. The exception is labels having only one possible genotype, where an existing
    # label can be reused.
    #
    # The crosses are taken from actual flowers in the farm, by weight.
    def add_random_rule(self, model, farm):
        farm_frequencies = {}
        for flower in farm.flowers:
            color = flower.label
            flowers = farm_frequencies.get(color, [])
            flowers.append(flower)
            farm_frequencies[color] = flowers

        farm_labels = list(farm_frequencies)
        # farm_weights = [len(farm_frequencies[x]) for x in farm_labels]
        cross = [random.choice(farm_labels), random.choice(farm_labels)]

        # cross = random.choices(farm_labels, weights=farm_weights, k=2)

        # Weight labels we have in the farm 2x
        # labels_weighted = list(self.labels) + farm_labels
        # cross = (random.choice(labels_weighted), random.choice(labels_weighted))
        # Self crosses are usually nice
        if random.random() < 0.2:
            cross[1] = cross[0]

        self.add_directed_rule(model, cross)

    def add_directed_rule(self, model, cross):
        # cross = (random.choice(list(self.labels)), random.choice(list(self.labels)))
        if self.has_rule(cross):
            return
        if cross[0].endswith("Universal") or cross[1].endswith("Universal"):
            return
        # Returns only unique genotypes.
        _outputs = allCrossings(self.labels[cross[0]], self.labels[cross[1]])
        # when doing a cross with identical parents, we can't block self clones
        # so consider that too. note _outputs is a set.
        if cross[0] == cross[1]:
            for x in self.labels[cross[0]] + self.labels[cross[1]]:
                _outputs.add(x)
        outputs = {}
        for output in _outputs:
            color = model.color(output)
            genotypes = outputs.get(color, [])
            genotypes.append(output)
            outputs[color] = genotypes
        output_labels = []
        for color, genotypes in outputs.items():
            genotypes = list(genotypes)
            existing_label = None
            label = None
            if len(genotypes) == 1:
                # if is_seed_genotype(model, genotypes[0]):
                #    continue  # Skip recipes outputting seeds
                existing_label = self.get_genotype_label(genotypes[0])
            # elif len(genotypes) <= 2 and model.universalGenotype in genotypes:
            #    existing_label = "PinkUniversal"
            #    self.labels[existing_label] = genotypes
            if existing_label is not None:
                label = existing_label
            else:
                label = self.make_longer_label(color)
                if len(label) < 64:
                    self.labels[label] = genotypes
                else:
                    label = None
            if label is not None:
                output_labels.append(label)
        if len(output_labels) > 0:
            # rules that only output the same label as an input are
            # not unproductive, but so far they haven't helped.
            if len(output_labels) == 1 and (
                cross[0] == output_labels[0] or cross[1] == output_labels[0]
            ):
                return
            # Fix irrelevantLabels hint
            self.irrelevantLabels.discard(cross[0])
            self.irrelevantLabels.discard(cross[1])
            self.rules.append(CrossRule(cross[0], cross[1], *output_labels))

    def __repr__(self):
        toRet = []
        toRet += [
            str(x)
            for x in sorted(
                [x for x in self.rules], key=lambda x: sorted([x.input1, x.input2]),
            )
        ]
        toRet += sorted([str(x) for x in self.labels.items()])
        return "\n".join(toRet)


def make_optimized_recipe(model, recipe):
    toRet = OptimizedRecipe(model)
    while True:
        old_len = len(toRet.rules)
        for rule in recipe.rules:
            if rule.input1 in toRet.labels and rule.input2 in toRet.labels:
                toRet.add_directed_rule(model, (rule.input1, rule.input2))
        if len(toRet.rules) == old_len:
            break
    return toRet


def minimize_optimized_recipe(model, recipe, target):
    toRet = OptimizedRecipe(model)

    # Depth first search to select only the DAG subset of recipe that
    # is relevant for yielding target. relevantLabels is used to ensure
    # that we visit each label only once when a given input can be used in
    # multiple recipes.
    relevantLabels = set()
    visitStack = []
    visitStack.append(target)
    relevantLabels.add(target)
    while len(visitStack) > 0:
        visit = visitStack.pop()
        for rule in recipe.rules:
            if visit in rule.output:
                for x in [rule.input1, rule.input2]:
                    if x not in relevantLabels:
                        relevantLabels.add(x)
                        visitStack.append(x)

    # Depth first search to select only labels that are reachable from seed
    # We can get into the situation of unreachable labels if we are randomly
    # removing rules for minimization purposes
    reachableLabels = set([seed[0] for seed in model.seeds])
    while True:
        changed = False
        for rule in recipe.rules:
            if (
                rule.input1 in reachableLabels
                and rule.input2 in reachableLabels
                and rule.input1 in relevantLabels
                and rule.input2 in relevantLabels
            ):
                for output in rule.output:
                    if output not in reachableLabels:
                        reachableLabels.add(output)
                        changed = True
        if not changed:
            break

    # We need to keep more than the relevantLabels, we actually need to keep
    # track of all labels referenced by any rules we're keeping
    keepLabels = set(list(relevantLabels))
    for rule in recipe.rules:
        relevantRule = False
        for output in rule.output:
            if output in relevantLabels:
                relevantRule = True
        reachableRule = (
            rule.input1 in reachableLabels and rule.input2 in reachableLabels
        )
        if relevantRule and reachableRule:
            for output in rule.output:
                keepLabels.add(output)
            toRet.rules.append(rule)

    for label in keepLabels:
        if label not in relevantLabels:
            toRet.irrelevantLabels.add(label)
        # Non-conformant recipes (that dropped labels) handled:
        # But these will fail if we try to extend them.
        if label in recipe.labels:
            toRet.labels[label] = recipe.labels[label]

    return toRet


class bestRecipes:
    def __init__(self, checkpoint):
        # Keep the best N scoring recipes
        # (lower score is better)
        self.N = 20
        self.checkpoint = checkpoint
        try:
            self.topK = hkl.load(self.checkpoint)
            logging.info("Loaded best recipes from checkpoint.")
        except Exception as e:
            self.topK = []

    def add(self, score, recipe):
        if len(self.topK) >= self.N and self.topK[-1][0] < score:
            return
        newBest = False
        if len(self.topK) > 0 and self.topK[0][0] > score:
            newBest = True
        self.topK = list(sorted(self.topK + [(score, recipe)], key=lambda x: x[0]))[
            0 : self.N
        ]
        logging.info(self)
        logging.info("Saving to checkpoint...")
        tmpfile = f"{self.checkpoint}~"
        hkl.dump(self.topK, tmpfile)
        os.system(f"mv {tmpfile} {self.checkpoint}")
        logging.info(f"Latest checkpoint is in {self.checkpoint}")

    def __repr__(self):
        toRet = [""]
        for score, recipe in reversed(self.topK):
            toRet += [f"===== {score} ====="]
            toRet += [str(recipe)]
        return "\n".join(toRet)


class RoseRecipe1:
    def __init__(self):
        self.rules = [
            CrossRule("Red", "Yellow", "Orange"),
            CrossRule("White", "White", "Purple"),
            CrossRule("Orange", "Purple", "RedHybrid"),
            CrossRule("RedHybrid", "RedHybrid", "Blue"),
        ]


class RoseRecipe2:
    def __init__(self):
        self.rules = [
            CrossRule("Red", "Yellow", "Orange"),
            CrossRule("White", "White", "Purple", "WhiteHybrid"),
            CrossRule("White", "Yellow", "WhiteHybridHybrid"),
            CrossRule("WhiteHybridHybrid", "Purple", "PurpleHybrid"),
            CrossRule("Orange", "PurpleHybrid", "OrangeHybrid", "RedHybrid"),
            CrossRule("RedHybrid", "RedHybrid", "Blue", "RedHybridHybrid"),
            CrossRule("OrangeHybrid", "OrangeHybrid", "Blue", "RedHybridHybridHybrid"),
            CrossRule("RedHybridHybridHybrid", "RedHybridHybridHybrid", "Blue"),
        ]


class uaverageidolRoseRecipe:
    def __init__(self):
        self.rules = [
            CrossRule("Red", "Yellow", "Orange", "WhiteHybrid"),
            CrossRule("Red", "Red", "WhiteHybridHybrid"),
            CrossRule("White", "White", "Purple"),
            CrossRule("WhiteHybrid", "Purple", "PurpleHybrid"),
            CrossRule("Orange", "Purple", "OrangeHybrid"),
            CrossRule("OrangeHybrid", "OrangeHybrid", "Blue", "RedHybridHybrid"),
            CrossRule("RedHybrid", "RedHybrid", "Blue"),
        ]


class RoseRecipe3:
    def __init__(self):
        self.rules = [
            CrossRule("Red", "Red", "Black"),
            CrossRule("Black", "Yellow", "Orange"),
            CrossRule("White", "White", "Purple"),
            CrossRule("White", "Yellow", "WhiteHybrid"),
            CrossRule("WhiteHybrid", "Purple", "PurpleHybrid"),
            CrossRule("Orange", "PurpleHybrid", "OrangeHybrid", "RedHybrid"),
            CrossRule("RedHybrid", "RedHybrid", "Blue"),
            CrossRule("OrangeHybrid", "OrangeHybrid", "Blue", "RedHybridHybrid"),
            CrossRule("RedHybridHybrid", "RedHybridHybrid", "Blue"),
        ]


class TulipRecipe:
    def __init__(self):
        self.rules = [
            CrossRule(
                # Can't get white or red here.
                # Orange is a strict improvement.
                "Red",
                "Yellow",
                # "YellowHybrid",
                "Orange",
            ),
            CrossRule("Orange", "Orange", "Purple"),
            # CrossRule("Orange", "Yellow", "OrangeHybrid"),
            # CrossRule("OrangeHybrid", "OrangeHybrid", "Purple"),
        ]


class TulipRecipe2:
    def __init__(self):
        self.rules = [
            CrossRule("Red", "Red", "Black",),
            CrossRule("Black", "Yellow", "Orange",),
            CrossRule("Orange", "Orange", "Purple"),
            # CrossRule("Orange", "Yellow", "OrangeHybrid"),
            # CrossRule("OrangeHybrid", "OrangeHybrid", "Purple"),
        ]


class PalehRoseRecipe:
    def __init__(self):
        self.rules = [
            CrossRule("White", "White", "Purple"),
            CrossRule("Purple", "Yellow", "WhiteHybridHybrid"),
            CrossRule("WhiteHybridHybrid", "Purple", "PurpleHybrid"),
            CrossRule("PurpleHybrid", "PurpleHybrid", "WhiteHybridHybridHybridHybrid"),
            CrossRule("Red", "Red", "Black"),
            CrossRule("WhiteHybridHybridHybridHybrid", "Black", "RedHybrid"),
            CrossRule("RedHybrid", "RedHybrid", "Blue"),
            CrossRule("RedHybrid", "WhiteHybridHybridHybridHybrid", "OrangeHybrid"),
            CrossRule(
                "OrangeHybrid", "WhiteHybridHybridHybridHybrid", "RedHybridHybridHybrid"
            ),
            CrossRule("RedHybridHybridHybrid", "RedHybridHybridHybrid", "Blue"),
        ]
