import numpy as np
import csv
from animalcrossing_hybrids.flower_models import *
from animalcrossing_hybrids.recipes import *
from animalcrossing_hybrids.flower import *
from animalcrossing_hybrids.cross_util import allCrossings
import logging
import argparse
import itertools

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description="Get all possible crosses.")
parser.add_argument("-m", "--mode", required=True, help="all or denseduplicators")
parser.add_argument("-o", "--out", required=True, help="Output file")
parser.add_argument("-f", "--flower", required=False, help="Output file")

args = parser.parse_args()

flowerTypes = [
    "Rose",
    "Tulip",
    "Cosmos",
    "Hyacinth",
    "Yuri",
    "Anemones",
    "Mum",
    "Pansy",
]
if args.flower:
    flowerTypes = [args.flower]

results = []
with open(args.out, "w") as tsvout:
    writer = csv.writer(tsvout, delimiter="\t")
    for flowerType in flowerTypes:
        model = FlowerModel(flowerType)
        allgenes = list(model.geneToColor)

        denseduplicators = set(model.colorToGenes)
        for i, a in enumerate(allgenes):
            for b in allgenes[i:]:
                # A set is returned.
                crossings = allCrossings([a], [b])
                colors = list(set([model.color(x) for x in crossings]))
                if model.color(a) == model.color(b):
                    if colors[0] != model.color(a) or len(colors) > 1:
                        # logging.info((model.color(a), model.color(b), a, b, colors))
                        denseduplicators.discard(model.color(a))

                for result in crossings:
                    writer.writerow(
                        (
                            a,
                            model.color(a),
                            b,
                            model.color(b),
                            result,
                            model.color(result),
                        )
                    )
        logging.info((flowerType, denseduplicators))

logging.info(results)
