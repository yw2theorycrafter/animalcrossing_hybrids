#!/bin/bash
#for flower in $*; do
for flower in Rose Tulip Pansy Anemones Mum Cosmos Hyacinth Yuri; do
    #python animalcrossing_hybrids/all_crossings.py -m all -f $flower -o out/$flower-allcrossings.tsv
    ./figures/crossing_graph.sh out/$flower-allcrossings.tsv data/$flower-hints.dot out/$flower-crossings $flower
    montage -geometry +0+0 out/$flower-crossings-*.png out/$flower-allcrossings.png
done
