#!/bin/bash
for file in $*; do
    filename="${file##*/}"
    base="${filename%.[^.]*}"
    sed 's/$/\t'$base'/' $file > out/tmp
    ./figures/composition.R out/tmp out/$base.pdf
done
