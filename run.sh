#!/bin/bash
set -beEu -o pipefail
FLOWER=$1
TARGET=$2
python -m animalcrossing_hybrids.simulator -o out/$FLOWER-$TARGET -f $FLOWER -t $TARGET -nsims 100 -m optimize
