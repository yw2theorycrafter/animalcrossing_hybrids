#!/bin/bash
(
for file in $*; do
    filename="${file##*/}"
    base="${filename%.[^.]*}"
    sed 's/$/\t'$base'/' $file
done
) > out/figure_input.tsv
./figures/timedistribution.R out/figure_input.tsv
