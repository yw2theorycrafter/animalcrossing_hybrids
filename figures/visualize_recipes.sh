#!/bin/bash
set -beEu -o pipefail
for file in $*; do
    base="${file%.*}"
    rm -f $base/*
    python -m figures._visualize_recipe $file
    function process_dot {
        base2="${1%.*}"
        dot -Tpdf "$1" -o "$base2.pdf"
        convert -density 150 "$base2.pdf" -background white -alpha remove -alpha off "$base2.png"
    }
    export -f process_dot
    parallel -j 4 process_dot {} ::: $base/*.dot
    montage -label 'takes < %t days 95% of time' -geometry +0+0 -pointsize 28 -bordercolor white -frame 0x5 $base/*.png $base.png
done
montage -filter Lanczos -geometry "1000x1000>+0+0" out/*-best-recipes.png out/best-recipes-ACNH.png
