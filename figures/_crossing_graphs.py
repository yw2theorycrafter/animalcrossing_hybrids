import logging
import csv
import argparse
import random

from animalcrossing_hybrids.flower import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

parser = argparse.ArgumentParser(description="Convert crossing table to graphvis DOT.")
parser.add_argument("-i", "--input", required=True, help="Flower offspring info")
parser.add_argument("-p", "--imageprefix", required=True, help="Prefix for images")
parser.add_argument("-n", "--nodedot", required=False, help="DOT format node layout")
parser.add_argument("-o", "--out", required=True, help="Output file")

args = parser.parse_args()

# Directed.
class Edge:
    def __init__(self, a, bColor, offspring):
        self.a = a
        self.bColor = bColor
        # Genotypes of b that go from a -> offspring
        self.bs = set()
        self.offspring = offspring


edges = {}

# Maps colors to all genotypes giving it
colors = {}


def addEdge(a, b, bColor, offspring, offspringGenes):
    colorGenes = colors.get(offspring, set())
    colorGenes.add(offspringGenes)
    colors[offspring] = colorGenes
    edgeKey = f"{a}{bColor}{offspring}"
    edge = edges.get(edgeKey, None)
    if not edge:
        edge = Edge(a, bColor, offspring)
    edge.bs.add(b)
    edges[edgeKey] = edge


with open(args.input, "r") as tsvin:
    reader = csv.reader(tsvin, delimiter="\t")
    for row in reader:
        addEdge(row[1], row[2], row[3], row[5], row[4])
        addEdge(row[3], row[0], row[1], row[5], row[4])


for color in colors:
    i = 0
    subgraphs = {}
    with open(f"{args.out}-{color}.dot", "w") as out:
        out.write("digraph G {\n")
        out.write("scale=5;\n")
        if args.nodedot:
            with open(args.nodedot) as nodedot:
                out.write(nodedot.read())
        for _c in colors:
            extranote = ""
            size = 40
            otherparams = ""
            if _c == color:
                size = 240
                extranote = "<br/>".join(colors[color])
                extranote = f'<td width="40">{extranote}</td>'
                otherparams = ", width=5, height=5, fixedsize=true"
            out.write(
                f'{_c} [label=<<table border="0"><tr><td height="{size}" width="{size}" fixedsize="true" ><img scale="true" src="{args.imageprefix}{_c}.png"/></td>{extranote}</tr></table>>, shape=circle, color=white {otherparams}];\n'
            )
        for edge in filter(
            lambda x: x.offspring == color and x.a != color and x.bColor != color,
            edges.values(),
        ):
            i += 1
            sg = subgraphs.get(edge.a, [])
            sg += [str(i)]
            subgraphs[edge.a] = sg
            edgelabel = (
                "<br/>".join(edge.bs) if len(edge.bs) <= 3 else f"{len(edge.bs)} ways"
            )
            edgelabel = f'<table border="0"><tr><td height="40" width="40" fixedsize="true" ><img src="{args.imageprefix}{edge.bColor}.png"/></td><td>{edgelabel}</td></tr></table>'
            out.write(f"{i} [label=<{edgelabel}>,shape=plain];\n")
            out.write(f"{edge.a} -> {i} [arrowhead=none];\n")
            out.write(f"{i} -> {edge.offspring};\n")
        i = 0
        for sg in subgraphs.values():
            i += 1
            out.write(f"subgraph g{i}" + "{" + ";".join(sg) + " }\n")
        out.write("}\n")
