import numpy as np
import regex as re
from animalcrossing_hybrids.flower_models import *
from animalcrossing_hybrids.recipes import *
from animalcrossing_hybrids.flower import *
from animalcrossing_hybrids.simulation_util import *
from multiprocessing import Pool
import logging
import argparse
from itertools import cycle
import hickle as hkl
import os
from tqdm import tqdm
from pathlib import Path

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

logger.setLevel(logging.INFO)

parser = argparse.ArgumentParser(
    description="Visualize best recipes for difficult flower crosses"
)
parser.add_argument("files", nargs="*", help="hdf5 files produced from optimization")

args = parser.parse_args()

for file in args.files:
    filepath = Path(file)
    best = bestRecipes(file)
    # for score, recipe in best.topK:
    # Grab the best recipe:
    for score, recipe in [best.topK[0]]:
        logging.info(recipe)
        outDir = str(filepath.parent / filepath.stem)
        os.makedirs(outDir, exist_ok=True)
        dotfile = str(Path(outDir) / f"{int(score)}.dot")
        if match := re.search("(.*?)-(.*?)-.*", filepath.stem):
            flowertype = match.group(1)
            target = match.group(2)
        else:
            raise RuntimeException("Invalid filename format")
        imageprefix = f"static/img/Flw{flowertype}"
        model = FlowerModel(flowertype)

        # Same trick as in optimization:
        if target == "Universal":
            target = f"{model.universalColor}Universal"
        recipe = minimize_optimized_recipe(model, recipe, target)

        # Get crossing data
        crossings = []
        for i in tqdm(range(1000)):
            successSteps, numFlowersWatered, farm, _crossings = simulation_runonce(
                model, recipe, "ImpossibleTarget", 200
            )
            crossings += _crossings
        crossing_frequency = {}
        for a, b, c in crossings:
            key = f"{a.label}x{b.label}"
            counts = crossing_frequency.get(key, {})
            count = counts.get(c, 0) + 1
            counts[c] = count
            crossing_frequency[key] = counts

        def offspring_fraction(a, b, c):
            key = f"{a}x{b}"
            counts = crossing_frequency.get(key, None)
            if counts is None:
                return ""
            total = sum([x for x in counts.values()])
            percentage = counts.get(c, 0) / total * 100
            return f" {percentage:.1f}% "

        with open(dotfile, "w") as out:
            out.write("digraph G {\n")
            out.write("scale=3;\n")
            for label, genotypes in recipe.labels.items():
                extranote = "<br/>".join(genotypes)
                extranote = f"<td>{extranote}</td>"
                color = model.color(genotypes[0])
                out.write(
                    f'{label} [label=<<table border="0"><tr><td height="40" width="40" fixedsize="true" ><img src="{imageprefix}{color}.png"/></td>{extranote}</tr></table>>, shape=plain];\n'
                )
            # Put the seeds together
            # Note: there is some magic here with subgraphs whose names start with cluster_
            out.write(
                f"subgraph cluster_seeds"
                + "{"
                + ";".join([x[0] for x in model.seeds])
                + ' label="seeds"}\n'
            )

            for rule in recipe.rules:
                ruleNode = f"{rule.input1}x{rule.input2}"
                out.write(f"{ruleNode} [shape=point];\n")
                out.write(f"{rule.input1} -> {ruleNode} [arrowhead=none];\n")
                out.write(f"{rule.input2} -> {ruleNode} [arrowhead=none];\n")
                for output in rule.output:
                    if output in recipe.labels:
                        edge_pct = offspring_fraction(rule.input1, rule.input2, output)
                        out.write(f'{ruleNode} -> {output} [label="{edge_pct}"];\n')
            # i = 0
            # for sg in subgraphs.values():
            #    i += 1
            #    out.write(f"subgraph g{i}" + "{" + ";".join(sg) + " }\n")
            out.write("}\n")
