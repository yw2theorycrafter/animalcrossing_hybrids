#!/bin/bash
set -beEu -o pipefail
#python -m figures._crossing_graphs -i $1 -n $2 -o $3 -p static/img/Flw$4
python -m figures._crossing_graphs -i $1 -o $3 -p static/img/Flw$4
for file in $3*.dot; do
neato -Tpdf $file -o $file.pdf
convert -density 96 $file.pdf -background white -alpha remove -alpha off $file.png
done
